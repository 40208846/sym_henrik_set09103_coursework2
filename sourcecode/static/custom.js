// from http://scratch99.com/web-development/javascript/convert-bytes-to-mb-kb/
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};
$(function(){
    var pathname = window.location.pathname;
    $('a').each(function(){
        if($(this).attr('href') == pathname){
            $(this).parent().addClass('active')
        }
    });
    $('.size').each(function(){
        var value = parseInt($(this).text());
        $(this).text(bytesToSize(value));
    });
    $('.time').each(function(){
        var value = parseInt($(this).text());
        var date = new Date(value * 1000)
        $(this).text(date.toUTCString().substr(0,16));
    });

})

