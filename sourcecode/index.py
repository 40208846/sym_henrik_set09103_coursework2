# -*- coding: utf-8 -*-
import os
from flask import Flask, render_template, abort, request, flash, redirect, url_for, current_app, send_from_directory
import json
from werkzeug.utils import secure_filename
from werkzeug import generate_password_hash, check_password_hash
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from flask_sqlalchemy import SQLAlchemy

# Folder uploaded files will be stored in
UPLOAD_FOLDER = './var'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# Setting up the app
app = Flask(__name__)
app.jinja_env.add_extension('jinja2.ext.do')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.db'
db = SQLAlchemy(app)
login_manager = LoginManager()


# Class used to represent the database for the users
# used to create new users and verify logging in users
class User(db.Model):
    __tablename__ = 'user'
    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<Email %r>' % self.email

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.email


# Class used to represent the database used to hold the list of files uploaded by a user
class UserFile(db.Model):
    __tablename__ = 'files'
    email = db.Column(db.String)
    filename = db.Column(db.String, primary_key=True)

    def __repr__(self):
        return '<Filename %r>' % self.filename

    def get_id(self):
        return self.email


# Form displayed when logging in
class LoginForm(Form):
    email = TextField('Email Address', [validators.Email(), validators.Required()])
    password = PasswordField('Password', [validators.Required()])


# Form displayed when registering
class RegisterForm(Form):
    email = TextField('Email Address', [validators.Email(), validators.Required()])
    password = PasswordField('Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')


# Given a user id, return the user from the database
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


# The index of the web app
@app.route('/')
def index():
    return render_template('index.html')


# Route to register a new user
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User()
        user.email = form.email.data
        user.password = generate_password_hash(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Successfully registered!')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


# Route for a user to log in with
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.query.get(form.email.data)
        if user:
            # Check the submitted password's hash matches stored hash
            if check_password_hash(user.password, form.password.data):
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user)
                flash('Logged in successfully')
                next = request.args.get('next')
                return redirect(next or url_for('index'))
            else:
                flash('Email/Password incorrect')
    return render_template('login.html', form=form)


# Checks file being uploaded has an allowed file extension
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Route to logout current user, user must be logged in
@app.route("/logout")
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return redirect(url_for('index'))


# Route to list of user's files
@app.route('/files')
@login_required
def files():
    user = current_user
    # Get a list of the user's files from the database, add these to a list of dictionaries 
    # of filenames and information about the files
    db_list = UserFile.query.filter_by(email=user.email).all()
    file_list = []
    for item in db_list:
        file_list.append({'name': item.filename, 'info': os.stat(item.filename)})
        print(file_list[-1]['info'].st_size)
    return render_template('files.html', file_list=file_list)


# Route allowing users to upload new files, user must be logged in
@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # Checks user has selected a file
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            tmp_file = UserFile()
            tmp_file.email = current_user.email
            tmp_file.filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            db.session.add(tmp_file)
            db.session.commit()
            return redirect(url_for('files'))
    return render_template('upload.html')


# Route allowing users to download their files, user must be logged in
@app.route('/download/<path:filename>', methods=['GET', 'POST'])
@login_required
def download(filename):
    folder = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
    return send_from_directory(directory=folder, filename=filename)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(401)
def page_not_found(e):
    return render_template('401.html'), 401

if __name__ == "__main__":
    # Key should be created on server when deploying, key provided here for example only
    app.secret_key = 'CHANGE THIS WHEN DEPLOYING'
    app.config['SESSION_TYPE'] = 'filesystem'
    login_manager.init_app(app)
    app.run(host='0.0.0.0')
